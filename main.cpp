#include <QApplication>
#include "ShotGridWidget.h"

int main(int argc, char *argv[])
{
   Q_INIT_RESOURCE(interview);

   QApplication app(argc, argv);

   ShotGridWidget shotGrid;
   shotGrid.setupModel(7);
   shotGrid.setupUi();
   shotGrid.show();

   return app.exec();
}

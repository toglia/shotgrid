#pragma once
#include <QListView>

class ShotGridSceneListView final : public QListView
{
   Q_OBJECT

public:
   explicit ShotGridSceneListView(QWidget *parent = Q_NULLPTR);
   ~ShotGridSceneListView();   

   void resizeEvent(QResizeEvent* event) override;
   void dragEnterEvent(QDragEnterEvent* event) override;
   void dragMoveEvent(QDragMoveEvent *event) override;
   void dropEvent(QDropEvent *event) override;
   void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected) override;

signals:
   void heightChanged(int height);

public slots:
   void elementsChanged();
   void rowsInsertedSlot(const QModelIndex& parent, int first, int last);
   void rowsRemovedSlot(const QModelIndex& parent, int first, int last);

private:
   int calculateElementsHeight() const;
};


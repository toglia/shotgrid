#pragma once

#include <QAbstractItemModel>
#include <QFileIconProvider>
#include <QIcon>
#include <QVector>

struct ShotGridItem final
{
   QString m_name;
   QString m_thumbnailPath;
};

class ShotGridViewModel : public QAbstractListModel
{
   Q_OBJECT

public:
   ShotGridViewModel(QString const& m_prefix, int rows, QObject *parent = nullptr);
   ~ShotGridViewModel();
   
   QModelIndex       index(int row, int column, const QModelIndex &parent) const override;
   QModelIndex       parent(const QModelIndex &child) const override;
   
   int               rowCount(const QModelIndex &parent) const override;
   int               columnCount(const QModelIndex &parent) const override;
   
   QVariant          data(const QModelIndex &index, int role) const override;
   QVariant          headerData(int section, Qt::Orientation orientation, int role) const override;
   
   bool              hasChildren(const QModelIndex &parent) const override;
   Qt::ItemFlags     flags(const QModelIndex &index) const override;
   Qt::DropActions   supportedDropActions() const override;
   QStringList       mimeTypes() const override;
   QMimeData*        mimeData(const QModelIndexList& indexes) const override;
   bool              dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent) override;

   bool              removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

   static QString    getMimeName();

private:
   ShotGridItem*                 getItem(int row) const;

   QVector<ShotGridItem*>        m_items;
   QModelIndex                   m_parent;

   QIcon                         m_serviceIcon;
   QFileIconProvider             m_iconProvider;
   QString                       m_prefix;

};


inline QString ShotGridViewModel::getMimeName()
{
   return "zonesense/shotgrid";
}



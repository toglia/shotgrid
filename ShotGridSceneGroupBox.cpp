#include "ShotGridSceneGroupBox.h"


ShotGridSceneGroupBox::ShotGridSceneGroupBox(QWidget* parent) :
   QGroupBox(parent)
{}

ShotGridSceneGroupBox::~ShotGridSceneGroupBox()
{}

void ShotGridSceneGroupBox::mouseReleaseEvent(QMouseEvent* event)
{
   emit clicked();
}

void ShotGridSceneGroupBox::updateHeight(int height)
{
   if (height > 0)
   {
      setMinimumHeight(height);
      setMaximumHeight(height);
   }
}


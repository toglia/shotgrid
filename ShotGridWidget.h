#pragma once

#include <QWidget>
#include <QItemSelection>
#include <QUndoCommand>

class ShotGridViewModel;
class ShotGridSceneListView;

struct ShotGridViewModelTuple
{
   QString              m_sceneName;
   ShotGridViewModel*   m_model;
};

class ShotGridWidget final : public QWidget
{
   Q_OBJECT

public:
   explicit ShotGridWidget(QWidget* parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
   ~ShotGridWidget();

   void setupUi();
   void setupModel(int sceneCount);
   void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
   void mousePressEvent(QMouseEvent *event) override;

public slots:
   void clearAllSelection();

private:
   QVector<ShotGridViewModelTuple>  m_models;
   QVector<ShotGridSceneListView*>  m_listViews;
   QItemSelectionModel              m_selectionModel;

   QAction*                         m_undoAction;
   QAction*                         m_redoAction;
   QAction*                         m_moveAction;
   std::unique_ptr<QUndoStack>      m_undoStack;



};




/********************************************************************************
** Form generated from reading UI file 'testlistoflists.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TESTLISTOFLISTS_H
#define UI_TESTLISTOFLISTS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QListView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_parent
{
public:
    QHBoxLayout *horizontalLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout;
    QGroupBox *box1;
    QVBoxLayout *boxLayout1;
    QListWidget *list1;
    QGroupBox *box2;
    QVBoxLayout *boxLayout2;
    QListView *list2;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *parent)
    {
        if (parent->objectName().isEmpty())
            parent->setObjectName(QString::fromUtf8("parent"));
        parent->resize(694, 648);
        parent->setSizeIncrement(QSize(1, 1));
        horizontalLayout = new QHBoxLayout(parent);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        scrollArea = new QScrollArea(parent);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 670, 624));
        verticalLayout = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        box1 = new QGroupBox(scrollAreaWidgetContents);
        box1->setObjectName(QString::fromUtf8("box1"));
        boxLayout1 = new QVBoxLayout(box1);
        boxLayout1->setObjectName(QString::fromUtf8("boxLayout1"));
        list1 = new QListWidget(box1);
        list1->setObjectName(QString::fromUtf8("list1"));
        list1->setMinimumSize(QSize(0, 300));
        list1->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        list1->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        boxLayout1->addWidget(list1);


        verticalLayout->addWidget(box1);

        box2 = new QGroupBox(scrollAreaWidgetContents);
        box2->setObjectName(QString::fromUtf8("box2"));
        boxLayout2 = new QVBoxLayout(box2);
        boxLayout2->setObjectName(QString::fromUtf8("boxLayout2"));
        list2 = new QListView(box2);
        list2->setObjectName(QString::fromUtf8("list2"));

        boxLayout2->addWidget(list2);


        verticalLayout->addWidget(box2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        scrollArea->setWidget(scrollAreaWidgetContents);

        horizontalLayout->addWidget(scrollArea);


        retranslateUi(parent);

        QMetaObject::connectSlotsByName(parent);
    } // setupUi

    void retranslateUi(QWidget *parent)
    {
        parent->setWindowTitle(QCoreApplication::translate("parent", "Form", nullptr));
        box1->setTitle(QCoreApplication::translate("parent", "GroupBox", nullptr));
        box2->setTitle(QCoreApplication::translate("parent", "GroupBox", nullptr));
    } // retranslateUi

};

namespace Ui {
    class parent: public Ui_parent {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TESTLISTOFLISTS_H

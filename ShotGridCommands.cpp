#include "ShotGridCommands.h"


MoveShotCommand::MoveShotCommand(MoveShotSnap new_snap, MoveShotSnap old_snap, QUndoCommand* parent)
{
   m_newSnap = new_snap;
   m_oldSnap = old_snap;
}

bool MoveShotCommand::mergeWith(const QUndoCommand* command)
{}

void MoveShotCommand::undo()
{}

void MoveShotCommand::redo()
{}

#pragma once
#include <QUndoCommand>

struct MoveShotSnap
{
   int m_sceneIndex;
   int m_shotIndex;
   int m_count;
};

class MoveShotCommand : public QUndoCommand
{
public:
   const int Id = 1234;

   MoveShotCommand(MoveShotSnap new_snap, MoveShotSnap old_snap, QUndoCommand* parent = nullptr);

   void  undo() override;
   void  redo() override;
   bool  mergeWith(const QUndoCommand* command) override;
   int   id() const override;

private:
   MoveShotSnap m_oldSnap;
   MoveShotSnap m_newSnap;   
};

inline int MoveShotCommand::id() const
{
   return Id;
}


#pragma once
#include <QGroupBox>

class ShotGridSceneGroupBox : public QGroupBox
{
   Q_OBJECT

public:
   explicit ShotGridSceneGroupBox(QWidget *parent = Q_NULLPTR);
   ~ShotGridSceneGroupBox();

   void	mouseReleaseEvent(QMouseEvent *event) override;

public slots:
   void updateHeight(int height);

signals:
   void clicked();


};



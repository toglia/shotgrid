#pragma once
#include <QScrollArea>

class ShotGridScrollArea final : public QScrollArea
{
   Q_OBJECT

public:
   explicit ShotGridScrollArea(QWidget *parent = Q_NULLPTR);
   ~ShotGridScrollArea();
};


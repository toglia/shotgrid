#include "ShotGridSceneListView.h"
#include <qevent.h>
#include <QMimeData>
#include "ShotGridViewModel.h"

ShotGridSceneListView::ShotGridSceneListView(QWidget* parent) :
   QListView(parent)
{
   setViewMode(QListView::IconMode);
   setSelectionMode(QAbstractItemView::ExtendedSelection);
   setAlternatingRowColors(false);
   setAttribute(Qt::WA_MacShowFocusRect, false);
   setResizeMode(QListView::Adjust);
   setUniformItemSizes(true);
   setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   setAcceptDrops(true);
   setDragEnabled(true);
   setDragDropMode(QAbstractItemView::DragDrop);
   setDefaultDropAction(Qt::MoveAction);
   setDropIndicatorShown(true);      
}

ShotGridSceneListView::~ShotGridSceneListView() = default;

int ShotGridSceneListView::calculateElementsHeight() const
{
   const auto rowHeight = sizeHintForRow(0) > 0 ? sizeHintForRow(0) : 80;
   const auto columnWidth = sizeHintForColumn(0) > 0 ? sizeHintForColumn(0) : 1;
   const auto itemCount = model()->rowCount();
   const auto currentSize = viewport()->size();
   const auto columnCount = std::floor(currentSize.width()/columnWidth);
   const auto itemRowCount = std::ceil(float(itemCount)/columnCount);
   return itemRowCount*rowHeight+rowHeight;
}

void ShotGridSceneListView::resizeEvent(QResizeEvent* event)
{
   QListView::resizeEvent(event);

   const auto newHeight = calculateElementsHeight();
   emit heightChanged(newHeight);
}

void ShotGridSceneListView::dragEnterEvent(QDragEnterEvent *event)
{
   auto m = static_cast<ShotGridViewModel*>(model());
   if (event->mimeData()->hasFormat(m->getMimeName()))
   {
      event->accept();
   }
   else
   {
      event->ignore();
   }
}

void ShotGridSceneListView::dragMoveEvent(QDragMoveEvent* event)
{
   auto m = static_cast<ShotGridViewModel*>(model());
   if (event->mimeData()->hasFormat(m->getMimeName()))
   {
      event->accept();
   }
   else
   {
      event->ignore();
   }
}

void ShotGridSceneListView::dropEvent(QDropEvent* event)
{
   const auto index = indexAt(event->pos());
   const auto row = index.isValid() ? index.row() : model()->rowCount();
   const auto result = model()->dropMimeData(
                           event->mimeData(), 
                           event->dropAction(), 
                           row, 0, QModelIndex());
   if (result)
   {
      event->accept();
   }
   else
   {
      event->ignore();
   }
}

void ShotGridSceneListView::selectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
   QListView::selectionChanged(selected, deselected);
}

void ShotGridSceneListView::elementsChanged()
{
   const auto newHeight = calculateElementsHeight();
   emit heightChanged(newHeight);
}


void ShotGridSceneListView::rowsInsertedSlot(const QModelIndex& parent, int first, int last)
{
   const auto newHeight = calculateElementsHeight();
   emit heightChanged(newHeight);

   for (auto i = first; i<last; ++i)
   {
      auto itemIndex = model()->index(i,0);
      //selectionModel()->select(itemIndex, QItemSelectionModel::Select);
   }
}

void ShotGridSceneListView::rowsRemovedSlot(const QModelIndex& parent, int first, int last)
{
   const auto newHeight = calculateElementsHeight();
   emit heightChanged(newHeight);

   clearSelection();

   selectionModel()->clearCurrentIndex();
   selectionModel()->clearSelection();
   update();
   updateGeometry();   
}




#include "ShotGridWidget.h"

#include <QVBoxLayout>
#include <QAction>

#include "ShotGridScrollArea.h"
#include "ShotGridViewModel.h"
#include "ShotGridSceneGroupBox.h"
#include "ShotGridSceneListView.h"

ShotGridWidget::ShotGridWidget(QWidget* parent, Qt::WindowFlags flags) :
   QWidget(parent, flags),
   m_undoStack(std::make_unique<QUndoStack>())
{}

ShotGridWidget::~ShotGridWidget()
{
   m_undoStack->clear();
}

void ShotGridWidget::setupUi()
{
   resize(640,480);

   auto scrollAreaLayout = new QVBoxLayout(this);
   auto scrollArea = new ShotGridScrollArea(this);   
   scrollArea->setWidgetResizable(true);

   const auto contents = new QWidget();
   auto contentLayout = new QVBoxLayout(contents);
   
   for (auto i = 0; i< m_models.size(); ++i)
   {
      auto& tuple = m_models[i];
      tuple.m_model->setParent(scrollArea);
      
      const auto groupBox = new ShotGridSceneGroupBox(contents);
      groupBox->setTitle(tuple.m_sceneName);

      auto boxLayout = new QVBoxLayout(groupBox);
      auto list = new ShotGridSceneListView(groupBox);
      list->setModel(tuple.m_model);
      m_listViews.push_back(list);

      boxLayout->addWidget(list);
      contentLayout->addWidget(groupBox);
      
      connect(list, &ShotGridSceneListView::heightChanged, groupBox, &ShotGridSceneGroupBox::updateHeight);
      connect(tuple.m_model, &ShotGridViewModel::rowsInserted, list, &ShotGridSceneListView::rowsInsertedSlot);
      connect(tuple.m_model, &ShotGridViewModel::rowsRemoved, list, &ShotGridSceneListView::rowsRemovedSlot);
      //::connect(list, &ShotGridSceneListView::droppedSuccessful, this, &ShotGridWidget::clearAllSelection);

      connect(list->selectionModel(), &QItemSelectionModel::selectionChanged, this, &ShotGridWidget::selectionChanged);
      connect(groupBox, &ShotGridSceneGroupBox::clicked, list, &ShotGridSceneListView::selectAll);

      connect(list->selectionModel(), &QItemSelectionModel::selectionChanged, this, &ShotGridWidget::selectionChanged);
   }

   const auto spacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
   contentLayout->addItem(spacer);  

   scrollArea->setWidget(contents);
   scrollAreaLayout->addWidget(scrollArea);

   setWindowIcon(QPixmap(":/images/interview.png"));
   setWindowTitle("Interview");


   m_undoAction = m_undoStack->createUndoAction(this, tr("&Undo"));
   m_undoAction->setShortcuts(QKeySequence::Undo);

   m_redoAction = m_undoStack->createRedoAction(this, tr("&Redo"));
   m_redoAction->setShortcuts(QKeySequence::Redo);
}

void ShotGridWidget::setupModel(int sceneCount)
{
   m_models.reserve(sceneCount);
   for (auto i=0; i<sceneCount;++i)
   {
      const auto itemCount = (rand()%100)+1;
      auto prefix = QString("S%1").arg(i);

      ShotGridViewModelTuple tuple;
      tuple.m_sceneName = QString("Scene %1").arg(QString::number(i));
      tuple.m_model = new ShotGridViewModel(prefix, itemCount);
      m_models.push_back(tuple);
   }
}

void ShotGridWidget::selectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
   const auto currentModel = !selected.isEmpty() ? selected.at(0).model() : nullptr;
   if(!currentModel)
   {
      return;
   }

   for (auto list : m_listViews)
   {
      const auto listModel = list->model();

      if (currentModel == listModel)
      {
         continue;
      }

      if (list->selectionModel()->hasSelection()) 
      {
         list->selectionModel()->clearSelection();
      }
   }
}


void ShotGridWidget::mousePressEvent(QMouseEvent* event)
{
   clearAllSelection();
}

void ShotGridWidget::clearAllSelection()
{
   for (auto listView : m_listViews)
   {
      listView->clearSelection();
   }
}


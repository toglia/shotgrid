#pragma once

#include "ShotGridCommands.h">

class QUndoCommand;

class ShotGridManager final 
{
public:
   ShotGridManager();
   ~ShotGridManager();
   ShotGridManager(const ShotGridManager&) = delete;
   ShotGridManager& operator=(const ShotGridManager&) = delete;

   void MoveItems(MoveShotSnap const& from, MoveShotSnap const& to);



};


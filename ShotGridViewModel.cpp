#include "ShotGridViewModel.h"

#include <QIcon>
#include <QPixmap>
#include <cassert>
#include <QMimeData>

ShotGridViewModel::ShotGridViewModel(QString const& prefix, int rows, QObject *parent)
    : QAbstractListModel(parent),
      m_serviceIcon(QPixmap(":/images/services.png"))
{
   for (auto i = 0; i< rows; ++i)
   {
      const QString name = prefix + QString("%1").arg(i);
      const QString thumbnail = QString("thumbnail_%1.png").arg(i);
      auto item = new ShotGridItem{name,thumbnail};
      m_items.push_back(item);
   }
}

ShotGridViewModel::~ShotGridViewModel()
{}

QModelIndex ShotGridViewModel::index(int row, int column, const QModelIndex &parent) const
{
   Q_UNUSED(parent);
   if (row < m_items.size() && row >= 0 && column >= 0) 
   {
      const auto item = getItem(row);
      if (item)
      {
         return createIndex(row, column, item);
      }
   }
   return m_parent;
}

QModelIndex ShotGridViewModel::parent(const QModelIndex &child) const
{
   Q_UNUSED(child);
   return m_parent;
}

int ShotGridViewModel::rowCount(const QModelIndex &parent) const
{
   Q_UNUSED(parent);
   return m_items.size();
}

int ShotGridViewModel::columnCount(const QModelIndex &parent) const
{
   Q_UNUSED(parent);
   return 1;
}

QVariant ShotGridViewModel::data(const QModelIndex &index, int role) const
{
   if (!index.isValid())
   {
      return QVariant();
   }
   
   if (role == Qt::DisplayRole)
   {
      const auto item = static_cast<ShotGridItem*>(index.internalPointer());
      return QVariant(item->m_name);
   }
       
   if (role == Qt::DecorationRole) 
   {
      if (index.column() == 0)
      {
         return m_iconProvider.icon(QFileIconProvider::Folder);
      }
      return m_iconProvider.icon(QFileIconProvider::File);
   }
   return QVariant();
}

QVariant ShotGridViewModel::headerData(int section, Qt::Orientation orientation, int role) const
{
   if (role == Qt::DisplayRole)
   {
      return QString::number(section);
   }
       
   if (role == Qt::DecorationRole)
   {
      return QVariant::fromValue(m_serviceIcon);
   }
   
   return QAbstractItemModel::headerData(section, orientation, role);
}

bool ShotGridViewModel::hasChildren(const QModelIndex &parent) const
{
   Q_UNUSED(parent);

   return !m_items.empty();
}

Qt::ItemFlags ShotGridViewModel::flags(const QModelIndex &index) const
{
   if (!index.isValid())
   {
      return Qt::NoItemFlags;
   }

   const Qt::ItemFlags defaultFlags = QAbstractItemModel::flags(index);
   return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | defaultFlags;
}

ShotGridItem* ShotGridViewModel::getItem(int row) const
{
   if (row > m_items.size())
   {
      assert(true);
   }

   return m_items[row];
}

Qt::DropActions ShotGridViewModel::supportedDropActions() const
{
   return Qt::MoveAction;
}

QStringList ShotGridViewModel::mimeTypes() const
{
   QStringList types;
   types << getMimeName();
   return types;
}

QMimeData* ShotGridViewModel::mimeData(const QModelIndexList &indexes) const
{
   QMimeData *mimeData = new QMimeData();
   QByteArray encodedData;
   
   QDataStream stream(&encodedData, QIODevice::WriteOnly);
   
   foreach (QModelIndex index, indexes)
   {
      if (index.isValid()) 
      {
         const auto item = static_cast<ShotGridItem*>(index.internalPointer());
         stream << item->m_name << item->m_thumbnailPath;
      }
   }
   
   mimeData->setData(getMimeName(), encodedData);
   return mimeData;
}

bool ShotGridViewModel::dropMimeData(
   const QMimeData *data,
   Qt::DropAction action, 
   int row, 
   int column, 
   const QModelIndex &parent)
{
   if (action == Qt::IgnoreAction)
   {
      return true;
   }
   
   if (!data->hasFormat(getMimeName()))
   {
      return false;
   }
   
   if (column > 0)
   {
      return false;
   }
   
   int beginRow;
   if (row != -1)
   {
      beginRow = row;
   }
   else if (parent.isValid())
   {
      beginRow = parent.row();
   }
   else
   {
      beginRow = rowCount(m_parent);
   }
   
   QByteArray encodedData = data->data(getMimeName());
   QDataStream stream(&encodedData, QIODevice::ReadOnly);

   QVector<int> newRows;
   int currentRow = beginRow;
   while (!stream.atEnd()) 
   {
      QString name;
      QString thumbnailPath;
      stream >> name >> thumbnailPath;
      m_items.insert(currentRow, new ShotGridItem{name, thumbnailPath});
      newRows.push_back(currentRow);
      ++currentRow;
   }

   beginInsertRows(m_parent, beginRow, currentRow);
   endInsertRows();

   return true;
}

bool ShotGridViewModel::removeRows(int row, int count, const QModelIndex& parent)
{
   if (parent.isValid())
   {
      return false;
   }

   if (row >= m_items.size() || row + count <= 0)
   {
      return false;
   }

   m_items.remove(row, count);

   beginRemoveRows(m_parent, row, row + count - 1);
   endRemoveRows();
   
   return true;
}


